import React from "react";

import confLogo from "../../images/conf-logo.png";

export default function Badge(props) {

  const { firstName, lastName, jobTitle, twitter } = props;

  return (
    <div className="badge card shadow d-flex flex-column align-items-center p-0 ml-5">
      <div className="inside-badge-conf-logo-container pt-2 pb-2">
        <img
          src={confLogo}
          className="inside-badge-conf-logo"
          alt="The Conf Logo"
        />
      </div>
      <div className="card-body d-flex align-items-center">
        <img
          src="https://www.gravatar.com/avatar?d=mp"
          className="avatar-img ml-2 mr-2"
          alt="Avatar"
        />
        <div className="name-container d-flex flex-column ml-2">
          <h2>{firstName}</h2>
          <h2>{lastName}</h2>
        </div>
      </div>
      <div className="info-container p-3">
        <p>{jobTitle}</p>
        <span className="twitter">@{twitter}</span>
      </div>
      <div className="hashtag-container p-4">#TheConf</div>
    </div>
  );
}
