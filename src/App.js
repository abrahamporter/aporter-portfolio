import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";
import Home from "./pages/Home";
import Badges from "./pages/Badges/Badges"

function App() {
	return (
		<Router>
			<div className="app">
				<Switch>
					<Route exact path="/" component={Home} />
					<Route path="/badges" component={Badges} />
				</Switch>
			</div>
		</Router>
	);
}

export default App;
