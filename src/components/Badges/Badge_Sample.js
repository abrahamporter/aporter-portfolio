import React from "react";
import { Link } from "react-router-dom";
import { FaTwitter } from "react-icons/fa";

export default function Badge_Sample({ badge, match }) {
  return (
    <Link to={match.url + "/id/" + badge.id} className="text-decoration-none">
      <div className="badge-sample bg-white shadow-sm m-2 d-flex flex-row align-items-center">
        <div className="sample-accent-color"></div>
        <img
          src="https://www.gravatar.com/avatar?d=identicon"
          className="sample-avatar-img border"
          alt="avatar"
        />
        <div className="sample-content">
          <p className="font-weight-bold">
            {badge.firstName} {badge.lastName}
          </p>
          <p className="twitter">
            <FaTwitter />
            {" @" + badge.twitter}
          </p>
          <p className="sample-job-title">{badge.jobTitle}</p>
        </div>
      </div>
    </Link>
  );
}
