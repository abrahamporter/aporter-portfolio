import React from "react";
import profilePhoto from "../../images/aporter_profile.jpg";

export default function Home_Header() {
	return (
		<div className="text-center pb-2">
			<header className="container-fluid">
				<h1>Abraham Porter's Portfolio</h1>
				<h4>
					Software Engineer | Full Stack Developer with JavaScript Technologies
				</h4>
			</header>
			<img src={profilePhoto} className="profile-photo" alt="Abraham Porter" />
		</div>
	);
}
