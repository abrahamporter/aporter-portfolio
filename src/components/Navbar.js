import React from 'react';
import { Link } from 'react-router-dom';
import { BsBoxArrowInLeft } from 'react-icons/bs';
import { FaReact } from 'react-icons/fa';
import '../styles/Navbar.css';

export default function Navbar() {
	return (
		<div className="navbar-container">
			<Link to="/" className="back-home">
				<BsBoxArrowInLeft />
				<span> Back to Home</span>
			</Link>
			<FaReact className="react-logo" />
		</div>
	)
}
