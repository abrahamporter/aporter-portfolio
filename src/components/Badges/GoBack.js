import React from "react";
import { IoReturnUpBack } from "react-icons/io5";

export default function GoBack({ history }) {
  return (
    <div onClick={history.goBack} className="go-back pl-5 mb-5">
      <IoReturnUpBack /> Back
    </div>
  );
}
