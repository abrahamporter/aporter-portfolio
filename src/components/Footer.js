import React from "react";
import { FaBitbucket, FaLinkedin } from "react-icons/fa";

export default function Footer() {
	return (
		<footer className="footer row p-2">
			<div className="social-link col-12 col-sm-4">
				<a href="https://bitbucket.org/abrahamporter/" target="_blank" rel="noreferrer">
					<FaBitbucket />
					<span className="ml-2">Bitbucket</span>
				</a>
			</div>
			<div className="social-link col-12 col-sm-4">
				<a href="https://www.linkedin.com/in/abraham-porter/" target="_blank" rel="noreferrer">
					<FaLinkedin />
					<span className="ml-2">LinkedIn</span>
				</a>
			</div>
			<div className="social-link col-12 col-sm-4">
				<a href="https://bio.torre.co/en/aporter" target="_blank" rel="noreferrer">
					<span>Torre Professional Genome</span>
				</a>
			</div>
		</footer>
	);
}
