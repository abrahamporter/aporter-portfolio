import React from "react";
import "../styles/Home.css";
import Header from "../components/Home/Home_Header";
import ProjectList from "../components/Home/Home_ProjectList";
import Footer from "../components/Footer";

export default function Home() {
  return (
    <div className="home-container">
      <Header />
      <ProjectList />
      <Footer />
    </div>
  );
}
