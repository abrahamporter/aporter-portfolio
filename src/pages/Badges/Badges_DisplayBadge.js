import React from "react";
import { useParams, useHistory } from "react-router-dom";

export default function Badges_DisplayBadge() {
  let { id } = useParams();
  let history = useHistory();

  return (
    <div>
      Displaying the badge with id: {id}
      <span onClick={history.goBack}>GoBack</span>
    </div>
  );
}
