import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import GoBack from "../../components/Badges/GoBack";
import Badge from "../../components/Badges/Badge";
import Form from "../../components/Badges/Badge_Form";

export default function Badges_AddEditBadge() {
  let history = useHistory();

  const [form, setForm] = useState({
    email: "",
    firstName: "",
    lastName: "",
    jobTitle: "",
    twitter: "",
  });

  const handleChange = (e) => {
    let newForm = { ...form, [e.target.name]: e.target.value };
    setForm(newForm);
  };

  return (
    <React.Fragment>
      <GoBack history={history} />
      <div className="d-flex flex-row justify-content-around">
        <Badge
          firstName={form.firstName}
          lastName={form.lastName}
          jobTitle={form.jobTitle}
          twitter={form.twitter}
        />
        <Form onChange={handleChange} formValues={form} />
      </div>
    </React.Fragment>
  );
}
