import React from "react";
import { Switch, Route } from "react-router-dom";
import Navbar from "../../components/Navbar";
import Footer from "../../components/Footer"
import BadgesStart from "./Badges_Start";
import AllBadges from "./Badges_AllBadges";
import AddEditBadge from "./Badges_AddEditBadge";
import DisplayBadge from "./Badges_DisplayBadge";

import "../../styles/Badges.css";

export default function Badges({ match }) {
  return (
    <div className="badges-container">
      <Navbar />
      <Switch>
        <Route exact path={match.url} component={BadgesStart} />
        <Route exact path={match.url + "/all"} component={AllBadges} />
        <Route exact path={match.url + "/new"} component={AddEditBadge} />
        <Route exact path={match.url + "/edit"} component={AddEditBadge} />
        <Route exact path={match.url + "/id/:id"} component={DisplayBadge} />
      </Switch>
			<Footer />
    </div>
  );
}
