import React from "react";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";
import GoBack from "../../components/Badges/GoBack";
import Sample from "../../components/Badges/Badge_Sample";

export default function Badges_AllBadges({ match }) {
  let history = useHistory();

  const storedBadges = [
    {
      id: 1,
      firstName: "1First Name",
      lastName: "1Last Name",
      jobTitle: "1Job Title",
      twitter: "1Twitter",
    },
    {
      id: 2,
      firstName: "2First Name",
      lastName: "2Last Name",
      jobTitle: "2Job Title",
      twitter: "2Twitter",
    },
    {
      id: 3,
      firstName: "3First Name",
      lastName: "3Last Name",
      jobTitle: "3Job Title",
      twitter: "3Twitter",
    },
  ];

  return (
    <div>
      <GoBack history={history} />
      <div className="d-flex flex-column align-items-center">
        <Link to={"/badges/new"} className="btn badge-btn mb-2">
          Add Badge
        </Link>
        {storedBadges.map((badge) => (
          <Sample key={badge.id} badge={badge} match={match} />
        ))}
      </div>
    </div>
  );
}
