import React from "react";
import { Link } from "react-router-dom";
import { BsBoxArrowInRight } from "react-icons/bs";

export default function Home_ProjectSample({ project }) {
	const { name, description, route } = project;
	return (
		<li className="card shadow col-xs-11 col-sm-5 col-lg-3 m-1 mt-3 mb-3 p-3">
			<p className="text-center font-weight-bold">{name}</p>
			<p className="text-justify">{description}</p>
			<Link to={route} className="text-center link">
				Go to project <BsBoxArrowInRight />
			</Link>
		</li>
	);
}
