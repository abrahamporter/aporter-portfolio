import React from "react";
import ProjectSample from "./Home_ProjectSample";

export default function Home_ProjectList() {
  const projects = [
    {
      key: 1,
      name: "Dynamic Badges in React",
      description:
        "A simple excercise on React that shows propper use of state by dynamically updating componets in response to input data.",
      route: "/badges",
    },
    {
      key: 2,
      name: "Trivia game connected to API",
      description:
        "A simple game that connects to the Open Trivia API to present multiple questions and answers, letting the user chose from these answers to receive a final score.",
      route: "/trivia",
    },
  ];

  return (
    <div className="container p-4">
      <h4 className="text-center mb-5">
        Check out my projects built with React!
      </h4>
      <ul className="project-list row">
        {projects.map((project) => (
          <ProjectSample key={project.key} project={project} />
        ))}
      </ul>
    </div>
  );
}
