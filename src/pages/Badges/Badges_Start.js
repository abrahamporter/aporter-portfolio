import React from "react";
import { Link } from "react-router-dom";
import Badge from "../../components/Badges/Badge";

import confLogo from "../../images/conf-logo.png";

export default function Badges_Start({ match }) {
  return (
    <div className="d-flex justify-content-center">
      <div className="text-center m-5">
        <img src={confLogo} className="conf-logo" alt="TheConf" />
        <p className="mt-3">Print your badges!</p>
				<p className="mb-3">The easiest way<br/>to manage your conference.</p>
        <Link to={match.url + "/all"} className="btn badge-btn">Lets Start!</Link>
      </div>
			<Badge firstName="First Name" lastName="Last Name" jobTitle="Job Title" twitter="Twitter" />
    </div>
  );
}
