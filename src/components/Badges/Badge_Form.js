import React from "react";

export default function Badge_Form({ onChange, formValues }) {
  const { email, firstName, lastName, jobTitle, twitter } = formValues;

  const handleSave = (e) => {
    e.preventDefault();
  }

  return (
    <div className="badge-form card rounded pt-3">
      <form className="input-group d-flex flex-column align-items-center">
        <label className="m-1">First Name</label>
        <input
          onChange={onChange}
          className="rounded"
          type="text"
          name="firstName"
          value={firstName}
          maxLength="16"
        />
        <label className="m-1">Last Name</label>
        <input
          onChange={onChange}
          className="rounded"
          type="text"
          name="lastName"
          value={lastName}
          maxLength="16"
        />
        <label className="m-1">Job Title</label>
        <input
          onChange={onChange}
          className="rounded"
          type="text"
          name="jobTitle"
          value={jobTitle}
          maxLength="30"
        />
        <label className="m-1">Twitter</label>
        <input
          onChange={onChange}
          className="rounded"
          type="text"
          name="twitter"
          value={twitter}
          maxLength="20"
        />
        <label className="m-1">Email</label>
        <input
          onChange={onChange}
          className="rounded"
          type="text"
          name="email"
          value={email}
        />
        <button onClick={handleSave} className="btn badge-btn mt-4">Save</button>
      </form>
    </div>
  );
}
